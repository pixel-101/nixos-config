{ ... }:
{
  environment.shellAliases = {
    l = "ls -l";
    lh = "ls -lh";
    a = "ls -a";
    ah = "ls -ah";
    la = "ls -la";
    lah = "ls -lah";
    systemd-run = "systemd-run --setenv=SUDO_UID=$UID --setenv=SUDO_GID=$GID";
    run0 = "systemd-run --setenv=SUDO_UID=$UID --setenv=SUDO_GID=$GID --setenv=SUDO_USER=$USER --pty --same-dir";
  };
}
