{ ... }:
{
  users.mutableUsers = false;
  environment.persistence."/nix/host/state/ext/gnome".users.pixel = {
    directories = [
      ".local/share/gnome-control-center-goa-helper"
      ".local/share/gnome-settings-daemon"
      ".local/share/gnome-shell/extensions"
      ".local/share/gnome-boxes"
      ".local/share/evolution"
      ".local/share/folks"
      ".local/share/geary"
      ".local/share/nautilus"
      ".local/share/gvfs-metadata"
      ".config/evolution"
      ".config/goa-1.0"
      ".config/geary"
      ".config/enchant"
      ".local/share/epiphany"
      ".local/share/webkitgtk"
      ".local/share/gnome-photos"
      ".local/share/cartridges"
      ".cache/evolution" # Evolution Caches take to long to regen
      ".local/share/lollypop"
    ];

    files = [
      ".local/share/gnome-shell/application_state"
      ".config/org.gabmus.giara.json"
    ];
  };
}
