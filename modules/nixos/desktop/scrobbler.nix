{ pkgs, ... }:
{
  environment.systemPackages = [ pkgs.mpris-scrobbler ];

  systemd.user.services."mpris-scrobbler" = {
    enable = true;
    wantedBy = [ "default.target" ];
    environment = {
      XDG_DATA_HOME = "%h/.local/share";
    };
    script = "${pkgs.mpris-scrobbler}/bin/mpris-scrobbler";
  };
}
