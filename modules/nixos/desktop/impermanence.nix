{ ... }:
{
  imports = [ ./impermanence-desktops/gnome.nix ];

  users.mutableUsers = false;
  environment.persistence."/nix/host/state".directories = [
    "/var/lib/upower"
    "/var/lib/logmein-hamachi"
  ];
  environment.persistence."/nix/host/state".users.pixel = {
    directories = [
      "Camera"
      "Documents"
      "Desktop"
      "Downloads"
      "Games"
      "Music"
      "Misc-NoSync"
      "Pictures"
      "Programs"
      "Projects"
      "Public"
      "Sync"
      "Templates"
      "Videos"
      "VMs"
      ".mozilla/firefox"
      ".var/app"
      ".local/share/icons/hicolor"
      ".local/share/PrismLauncher"
      ".local/share/komikku"
      ".local/share/bottles"
      ".local/share/authenticator"
      ".local/share/mpris-scrobbler"
      ".local/share/applications"
      ".local/share/Steam"
      ".local/share/Terraria"
      ".local/share/Mindustry"
      ".local/share/shapez.io"
      ".local/share/TIS-100"
      ".factorio"
      ".steam"
      ".config/r2modmanPlus-local"
      ".config/gsconnect"
      ".config/libvirt"
      ".config/YouTube Music"
      ".local/state/home-manager/gcroots"
      ".local/state/nix/profiles"
      ".config/syncthing"
      ".local/.stfolder"
      ".config/.stfolder"
      ".config/obs-studio"
      ".wine"
      ".local/share/zoxide"
      ".vim"
      ".config/vesktop"
      ".local/share/osu"
      ".local/share/nix"
      ".local/share/Tabletop Simulator"
      ".config/unity3d"
      ".config/libreoffice"
      ".local/state/wireplumber"
      ".local/share/vulkan"
      ".local/share/FasterThanLight"
      ".kodi/userdata/Database"
      ".kodi/userdata/Savestates"
      ".kodi/userdata/Thumbnails"
      ".kodi/.stfolder"
      ".config/calibre"
      ".local/share/procalc"
      ".local/state/bash"
      ".config/steamtinkerlaunch"
      { directory = ".gnupg"; mode = "0700"; }
      { directory = ".local/share/keyrings"; mode = "0700"; }
    ];
  };
}
