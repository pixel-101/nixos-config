{ lib, pkgs, ... }:
{
  programs.steam = {
    package = pkgs.steam.override {
      extraLibraries = pkgs: [
        pkgs.openssl # Why did I need this again?

        # TF2 needs fixed libtcmalloc.so.4, shipped ones are broken
        # Add `LD_PRELOAD=/lib32/libtcmalloc.so.4 %command%` to launch args to fix
        # 64 bit build is fine without
        pkgs.pkgsi686Linux.gperftools
      ];
    };
    enable = true;
    gamescopeSession.enable = true;
    remotePlay.openFirewall = true;
  };
  programs.java.enable = true;
  programs.dconf.enable = true; # Fix issues in HM WMs
  programs.haguichi.enable = true;
  services.logmein-hamachi.enable = true;
  virtualisation = {
    containers.enable = true;
    podman.enable = true;
  };
}
