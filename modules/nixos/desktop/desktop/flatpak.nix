{ ... }:
{
  services.flatpak = {
    enable = true;
    packages = [
      # Version freeze to ease network traffic
      "flathub:app/com.steamgriddb.SGDBoop//stable:1126174281149a4ff02e9870c799a8aab66874c76192f107fdf8f848a43ed930"
    ];
    remotes = {
      flathub = "https://dl.flathub.org/repo/flathub.flatpakrepo";
    };
    state-dir = "/nix/store-flatpak";
    overrides = {
      global = {
        environment = {
          "MOZ_ENABLE_WAYLAND" = 1;
        };
        sockets = [
          "!x11"
          "fallback-x11"
        ];
      };
      "info.febvre.Komikku".environment = {
        "ICON_THEME" = "Adwaita";
      };
    };
  };
}
