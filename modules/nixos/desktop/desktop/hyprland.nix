{ lib, pkgs, ... }:
let
  sessions = lib.makeSearchPath "share/wayland-sessions" (with pkgs; [
    hyprland
  ]);
  greetdArgs = [ "-s" ] ++ (sessions) ++ [
    "--power-shutdown"
    "${pkgs.systemd}/bin/systemctl poweroff"
    "--power-reboot"
    "${pkgs.systemd}/bin/systemctl reboot"
    "--asterisks"
  ];
in
{
  # HM deals with hyprland, but I need the supporting modules that nixos provides
  # Maybe I will get duplacete packages?
  programs.hyprland.enable = true;

  # A nice login program, hyprland does not have one like gnome does
  services.greetd = {
    enable = true;
    settings = {
      default_session.command = "${pkgs.greetd.tuigreet}/bin/tuigreet" + (lib.concatStrings greetdArgs);
    };
  };
}
