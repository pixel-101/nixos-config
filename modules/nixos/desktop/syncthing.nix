{ ... }:
{
  services.syncthing = {
    enable = true;
    user = "pixel";
    dataDir = "/home/pixel";
    configDir = "/home/pixel/.config/syncthing";
    overrideDevices = true;
    overrideFolders = true;
    settings = {
      devices = {
        "pixels-laptop" = { id = "VXIF4LG-PT7WWYH-VNWUANZ-AFVC6GK-X3H7JKD-MGKQGGY-YGIYQHS-I277NAR"; };
        "pixels-pc" = { id = "G6QMWF4-LYW5T37-7NMTCDA-2B5PMUA-SNWHFAU-ZKWE77L-2EZACNM-PSL4BQJ"; };
        "phone" = { id = "OBXVJES-LQAL6QN-6ZSRTE2-Y62KSLH-Q3GDZNI-XUN6SCQ-AZHPDFX-KRJW5QM"; };
        "deck" = { id = "OFO7QBD-ERD47CK-YEMB4EA-2X7MPYB-SHRWJFK-YQ4GERM-56U2WI7-ITAMVA6"; };
      };
      folders = {
        "VMs" = {
          id = "virtual-machines";
          path = "~/VMs";
          devices = [ "pixels-laptop" "pixels-pc" ];
        };
        "Factorio" = {
          id = "facto-irio";
          path = "~/.factorio";
          devices = [ "pixels-laptop" "pixels-pc" ];
        };
        "Programs" = {
          id = "2e3t3-ryzen";
          path = "~/Programs";
          devices = [ "pixels-laptop" "deck" "pixels-pc" ];
        };
        "Games" = {
          id = "45aik-9ajyr";
          path = "~/Games";
          devices = [ "pixels-laptop" "pixels-pc" "deck" ];
        };
        "Templates" = {
          id = "5619q-wqx21";
          path = "~/Templates";
          devices = [ "pixels-laptop" "pixels-pc" ];
        };
        "Documents" = {
          id = "7t10j-5frar";
          path = "~/Documents";
          devices = [ "pixels-laptop" "pixels-pc" "deck" "phone" ];
        };
        "Music" = {
          id = "ly0bl-r2iqi";
          path = "~/Music";
          devices = [ "pixels-laptop" "pixels-pc" "deck" "phone" ];
        };
        "Pictures" = {
          id = "omyo0-dp5p0";
          path = "~/Pictures";
          devices = [ "pixels-laptop" "pixels-pc" "phone" ];
        };
        "Camera" = {
          id = "pixel_6_kjap-photos";
          path = "~/Camera";
          devices = [ "pixels-laptop" "pixels-pc" "phone" ];
        };
        "Videos" = {
          id = "qoy8g-azv4o";
          path = "~/Videos";
          devices = [ "pixels-laptop" "pixels-pc" "phone" ];
        };
        "Downloads" = {
          id = "rsejh-jym7t";
          path = "~/Downloads";
          devices = [ "pixels-laptop" "pixels-pc" "phone" ];
        };
        "Default Folder" = {
          id = "sn1nr-pcw8m";
          path = "~/Sync";
          devices = [ "pixels-laptop" "pixels-pc" "phone" "deck" ];
        };
        "Config" = {
          id = "local_config";
          path = "~/.config";
          devices = [ "pixels-laptop" "pixels-pc" "deck" ];
        };
        "Data" = {
          id = "local_data";
          path = "~/.local";
          devices = [ "pixels-laptop" "pixels-pc" "deck" ];
        };
        "Kodi" = {
          id = "local_kodi";
          path = "~/.kodi";
          devices = [ "pixels-laptop" "pixels-pc" ];
        };
        "Flatpak Data" = {
          id = "flatpak_data";
          path = "~/.var/app";
          devices = [ "pixels-laptop" "pixels-pc" ];
        };
      };
    };
  };
  networking.firewall.allowedTCPPorts = [ 8384 22000 ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];
}
